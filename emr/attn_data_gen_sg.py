import collections
import math
import time
import datetime
import numpy as np
import random
from numpy import random as npRdm,sqrt
import json

def build_dictionary(patients, min_cut_freq, only_dx_flag, dict_file, padding=True,sample=None):
    
    allCodes = [] #store all diagnosis codes
    total_words = 0
    for patient in patients:
        for visit in patient['visits']:
            
            DXs = visit['DXs']
            for dx in DXs:
                allCodes.append('D_'+dx)
            if not only_dx_flag:
                TXs = visit['CPTs']
                for tx in TXs:
                    allCodes.append('T_'+tx)
    
    #store all codes and corresponding counts
    count_org = []
    count_org.extend(collections.Counter(allCodes).most_common())
    
    #store filtering codes and counts
    if padding:
        count = [['PAD', -1]]
    else:
        count = []
    for word, c in count_org:
        word_tuple = [word, c]
        if c >= min_cut_freq:
            count.append(word_tuple)
            total_words += c
            
    if not sample:
        # no words downsampled
        threshold_count = total_words
    elif sample < 1.0:
        # traditional meaning: set parameter as proportion of total
        threshold_count = sample * total_words
    else:
        # new shorthand: sample >= 1 means downsample all words with higher count than sample
        threshold_count = int(sample * (3 + sqrt(5)) / 2)
    
    #create dictionary formating code:index
    dictionary = dict()
    wordSample = dict()
    for word, cnt in count:
        index = len(dictionary)
        dictionary[word] = index
#         print(cnt)
        word_probability = (sqrt(cnt / threshold_count) + 1) * (threshold_count / cnt)
        sample_int = int(round(word_probability * 2**32))
        wordSample[index] = int(sample_int)

    for patient in patients:
        visits = patient['visits']
        for visit in visits:
            DXs = visit['DXs']
            if len(DXs) == 0:
                continue
            else:
                visit['DXs'] = [dictionary['D_'+dx] for dx in DXs if 'D_'+dx in dictionary]
                
            if not only_dx_flag:
                TXs = visit['CPTs']
                if len(TXs) == 0:
                    continue
                else:
                    visit['CPTs'] = [dictionary['T_'+tx] for tx in TXs if 'T_'+tx in dictionary]
        
        
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    
    with open(dict_file+'.json', 'w') as fp:
        json.dump(reverse_dictionary, fp)
    return total_words,count, wordSample, dictionary, reverse_dictionary

def generate_sg_batch_position_gensim(patients, window, data_index, only_dx_flag, skip_window,sample_flag,wordSample):
   
    batch = list()
    labels = list()

    patient = patients[data_index]
    visits = patient['visits']
    num_visits = len(visits)
    visits_sorted = sorted(visits,key = lambda visit: visit['admsn_dt']) 
    
    if window >= num_visits:
        selected_visits = visits_sorted
        selected_codes = []
        
        for s_visit in selected_visits:
            codes = s_visit['DXs']
            if not only_dx_flag:
                codes.extend(s_visit['CPTs'])
            selected_codes.extend(codes)
            
        if sample_flag:
            selected_codes = [code for code in selected_codes if wordSample[code] > npRdm.rand() * 2 ** 32]
            
        for pos, word in enumerate(selected_codes):
            reduced_window = npRdm.RandomState(1).randint(skip_window)

            # now go over all words from the (reduced) window, predicting each one in turn
            start = max(0, pos - skip_window + reduced_window)
            for pos2, word2 in enumerate(selected_codes[start:(pos + skip_window + 1 - reduced_window)], start):
                # don't train on the `word` itself
                if pos2 != pos:
                    batch.append(word)
                    labels.append([word2])
                    
        return np.array(batch,dtype=np.int32),np.array(labels,dtype=np.int32)
    
    else:
#         print('number of visits:',num_visits)
        for pos, visit in enumerate(visits_sorted):

            window_pos = enumerate(visits_sorted[pos:(pos + window)], pos)
            selected_visits = [c_visit for pos2, c_visit in window_pos if (len(c_visit['DXs']) > 0)]
            selected_codes = []

            for s_visit in selected_visits:
                codes = s_visit['DXs']
                if not only_dx_flag:
                    codes.extend(s_visit['CPTs'])
                selected_codes.extend(codes)
                
            if sample_flag:
                selected_codes = [code for code in selected_codes if wordSample[code] > npRdm.rand() * 2 ** 32]

            for pos1, word in enumerate(selected_codes):
                reduced_window = npRdm.RandomState(1).randint(skip_window)

                # now go over all words from the (reduced) window, predicting each one in turn
                start = max(0, pos1 - skip_window + reduced_window)
                for pos2, word2 in enumerate(selected_codes[start:(pos1 + skip_window + 1 - reduced_window)], start):
                    # don't train on the `word` itself
                    if pos2 != pos1:
                        batch.append(word)
                        labels.append([word2])
            if pos + window >= num_visits:
                return np.array(batch,dtype=np.int32),np.array(labels,dtype=np.int32)

def generate_sg_cbow_batch_position_gensim(patients, window, data_index, only_dx_flag, skip_window,sample_flag,wordSample, mimi_batch):
   
    batches = []
    batches_list = []
    patient = patients[data_index]
    visits = patient['visits']
    num_visits = len(visits)

    visits_sorted = sorted(visits,key = lambda visit: visit['admsn_dt']) 
    
    if window >= num_visits:
        selected_visits = visits_sorted
        selected_codes = []
        
        for s_visit in selected_visits:
            codes = s_visit['DXs']
            if not only_dx_flag:
                codes.extend(s_visit['CPTs'])
            selected_codes.extend(codes)
            
        if sample_flag:
            selected_codes = [code for code in selected_codes if wordSample[code] > npRdm.rand() * 2 ** 32]
        
#         print(selected_codes)
        for pos, word in enumerate(selected_codes):
            
            window_sg_trains = []
            window_sg_labels = []
            window_cbow_trains = []
            window_cbow_labels = []
            
            reduced_window = npRdm.RandomState(1).randint(skip_window)
            # now go over all words from the (reduced) window, predicting each one in turn
            start = max(0, pos - skip_window + reduced_window)
            
#             print('reduced window: ',reduced_window)
            
#             print('codes in a window')
#             print(selected_codes[start:(pos + skip_window + 1 - reduced_window)])
            window_pos = enumerate(selected_codes[start:(pos + skip_window + 1 - reduced_window)], start)
            
            word2_indices = [word2 for pos2, word2 in window_pos if (word2 is not None and pos2 != pos)]
            window_cbow_trains.append(word2_indices)
            window_cbow_labels.append([word])
            
            for pos2, word2 in enumerate(selected_codes[start:(pos + skip_window + 1 - reduced_window)], start):
                # don't train on the `word` itself
                if pos2 != pos:
                    window_sg_trains.append(word)
                    window_sg_labels.append([word2])
            batches.append([np.array(window_sg_trains,dtype=np.int32),np.array(window_sg_labels,dtype=np.int32),
                            np.array(window_cbow_trains,dtype=np.int32),np.array(window_cbow_labels,dtype=np.int32)])
            batches_list.append([window_sg_trains,window_sg_labels,window_cbow_trains,window_cbow_labels])               
        
        
        batch_size = len(batches)
        newBatches = []
        lengths = []
        for i in range(batch_size):
            lengths.append(batches[i][0].shape[0])
        uniLengths = set(lengths)

        for length in uniLengths:
            sg_trains = []
            sg_labels = []
            cbow_trains = []
            cbow_labels = []
            batch_size = 0
            for batch in batches_list:
                if len(batch[0]) == length:
                    sg_trains.extend(batch[0])
                    sg_labels.extend(batch[1])
                    cbow_trains.extend(batch[2])
                    cbow_labels.extend(batch[3])
                    batch_size += 1
                    if batch_size % mimi_batch == 0:
                        newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                    np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)])
                        sg_trains = []
                        sg_labels = []
                        cbow_trains = []
                        cbow_labels = []

            newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                    np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)]) 
        return batches,newBatches
    
    else:
        for pos, visit in enumerate(visits_sorted):
            
            window_pos = enumerate(visits_sorted[pos:(pos + window)], pos)
            selected_visits = [c_visit for pos2, c_visit in window_pos if (len(c_visit['DXs']) > 0)]
            selected_codes = []

            for s_visit in selected_visits:
                codes = s_visit['DXs']
                if not only_dx_flag:
                    codes.extend(s_visit['CPTs'])
                selected_codes.extend(codes)
            
            if sample_flag:
                selected_codes = [code for code in selected_codes if wordSample[code] > npRdm.rand() * 2 ** 32]

            for pos1, word in enumerate(selected_codes):
                           
                window_sg_trains = []
                window_sg_labels = []
                window_cbow_trains = []
                window_cbow_labels = []
                           
                reduced_window = npRdm.RandomState(1).randint(skip_window)

                # now go over all words from the (reduced) window, predicting each one in turn
                start = max(0, pos1 - skip_window + reduced_window)
                window_pos = enumerate(selected_codes[start:(pos1 + skip_window + 1 - reduced_window)], start)
            
                word2_indices = [word2 for pos2, word2 in window_pos if (word2 is not None and pos2 != pos1)]
                window_cbow_trains.append(word2_indices)
                window_cbow_labels.append([word])           
                   
                for pos2, word2 in enumerate(selected_codes[start:(pos1 + skip_window + 1 - reduced_window)], start):
                    # don't train on the `word` itself
                    if pos2 != pos1:
                        window_sg_trains.append(word)
                        window_sg_labels.append([word2])
                batches.append([np.array(window_sg_trains,dtype=np.int32),np.array(window_sg_labels,dtype=np.int32),
                            np.array(window_cbow_trains,dtype=np.int32),np.array(window_cbow_labels,dtype=np.int32)])
                batches_list.append([window_sg_trains,window_sg_labels,window_cbow_trains,window_cbow_labels])
            if pos + window >= num_visits:
                
                batch_size = len(batches)
                newBatches = []
                lengths = []
                for i in range(batch_size):
                    lengths.append(batches[i][0].shape[0])
                uniLengths = set(lengths)

                for length in uniLengths:
                    sg_trains = []
                    sg_labels = []
                    cbow_trains = []
                    cbow_labels = []
                    batch_size = 0
                    for batch in batches_list:
                        if len(batch[0]) == length:
                            sg_trains.extend(batch[0])
                            sg_labels.extend(batch[1])
                            cbow_trains.extend(batch[2])
                            cbow_labels.extend(batch[3])
                            batch_size += 1
                            if batch_size % mimi_batch == 0:
                                newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                            np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)])
                                sg_trains = []
                                sg_labels = []
                                cbow_trains = []
                                cbow_labels = []

                    newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                            np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)]) 
                return batches,newBatches
            
def generate_sg_cbow_batch_attention(patients, window, data_index, only_dx_flag, skip_window,sample_flag,wordSample,mimi_batch):
   
    batches = []
    batches_list = []
    patient = patients[data_index]
    visits = patient['visits']
    num_visits = len(visits)

    visits_sorted = sorted(visits,key = lambda visit: visit['admsn_dt']) 
    
    if window >= num_visits:
        selected_visits = visits_sorted
        selected_codes = []
        
        for s_visit in selected_visits:
            dt = datetime.datetime.strptime(s_visit['admsn_dt'], "%Y%m%d")
            codes = s_visit['DXs']
            if not only_dx_flag:
                codes.extend(s_visit['CPTs'])
            for code in codes:
                selected_codes.append([code,dt])
            
        if sample_flag:
            selected_codes = [code for code in selected_codes if wordSample[code[0]] > npRdm.rand() * 2 ** 32]
            
        for pos, word in enumerate(selected_codes):
            
            window_sg_trains = []
            window_sg_labels = []
            window_cbow_trains = []
            window_cbow_labels = []
            
            reduced_window = npRdm.RandomState(1).randint(skip_window)
            # now go over all words from the (reduced) window, predicting each one in turn
            start = max(0, pos - skip_window + reduced_window)
            window_pos = enumerate(selected_codes[start:(pos + skip_window + 1 - reduced_window)], start)

            word2_indices = [[word2[0],np.abs((word2[1]-word[1]).days)] for pos2, word2 
                             in window_pos if (word2[0] is not None and pos2 != pos)]
#             print(word2_indices)
            window_cbow_trains.append(word2_indices)
            window_cbow_labels.append([word[0]])
            
            for pos2, word2 in enumerate(selected_codes[start:(pos + skip_window + 1 - reduced_window)], start):
                # don't train on the `word` itself
                if pos2 != pos:
                    window_sg_trains.append(word[0])
                    window_sg_labels.append([word2[0]])
            batches.append([np.array(window_sg_trains,dtype=np.int32),np.array(window_sg_labels,dtype=np.int32),
                            np.array(window_cbow_trains,dtype=np.int32),np.array(window_cbow_labels,dtype=np.int32)])
            batches_list.append([window_sg_trains,window_sg_labels,window_cbow_trains,window_cbow_labels])     
            
        
        batch_size = len(batches)
        newBatches = []
        lengths = []
        for i in range(batch_size):
            lengths.append(batches[i][0].shape[0])
        uniLengths = set(lengths)

        for length in uniLengths:
            sg_trains = []
            sg_labels = []
            cbow_trains = []
            cbow_labels = []
            batch_size = 0
            for batch in batches_list:
                if len(batch[0]) == length:
                    sg_trains.extend(batch[0])
                    sg_labels.extend(batch[1])
                    cbow_trains.extend(batch[2])
                    cbow_labels.extend(batch[3])
                    batch_size += 1
                    if batch_size % mimi_batch == 0:
                        newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                    np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)])
                        sg_trains = []
                        sg_labels = []
                        cbow_trains = []
                        cbow_labels = []

            newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                    np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)]) 
        return batches,newBatches
    
    else:
        for pos, visit in enumerate(visits_sorted):
            
            window_pos = enumerate(visits_sorted[pos:(pos + window)], pos)
            selected_visits = [c_visit for pos2, c_visit in window_pos if (len(c_visit['DXs']) > 0)]
            selected_codes = []

            for s_visit in selected_visits:
                dt = datetime.datetime.strptime(s_visit['admsn_dt'], "%Y%m%d")
                codes = s_visit['DXs']
                if not only_dx_flag:
                    codes.extend(s_visit['CPTs'])
                for code in codes:
                    selected_codes.append([code,dt])
            
            if sample_flag:
                selected_codes = [code for code in selected_codes if wordSample[code[0]] > npRdm.rand() * 2 ** 32]

            for pos1, word in enumerate(selected_codes):
                           
                window_sg_trains = []
                window_sg_labels = []
                window_cbow_trains = []
                window_cbow_labels = []
                           
                reduced_window = npRdm.RandomState(1).randint(skip_window)

                # now go over all words from the (reduced) window, predicting each one in turn
                start = max(0, pos1 - skip_window + reduced_window)
                window_pos = enumerate(selected_codes[start:(pos1 + skip_window + 1 - reduced_window)], start)
            
                word2_indices = [[word2[0],np.abs((word2[1]-word[1]).days)] for pos2, word2 
                             in window_pos if (word2[0] is not None and pos2 != pos1)]
            
                window_cbow_trains.append(word2_indices)
                window_cbow_labels.append([word[0]])

                for pos2, word2 in enumerate(selected_codes[start:(pos1 + skip_window + 1 - reduced_window)], start):
                    # don't train on the `word` itself
                    if pos2 != pos1:
                        window_sg_trains.append(word[0])
                        window_sg_labels.append([word2[0]])
                        
                batches.append([np.array(window_sg_trains,dtype=np.int32),np.array(window_sg_labels,dtype=np.int32),
                            np.array(window_cbow_trains,dtype=np.int32),np.array(window_cbow_labels,dtype=np.int32)])
                batches_list.append([window_sg_trains,window_sg_labels,window_cbow_trains,window_cbow_labels])
            
            if pos + window >= num_visits:
                
                batch_size = len(batches)
                newBatches = []
                lengths = []
                for i in range(batch_size):
                    lengths.append(batches[i][0].shape[0])
                uniLengths = set(lengths)

                for length in uniLengths:
                    sg_trains = []
                    sg_labels = []
                    cbow_trains = []
                    cbow_labels = []
                    batch_size = 0
                    for batch in batches_list:
                        if len(batch[0]) == length:
                            sg_trains.extend(batch[0])
                            sg_labels.extend(batch[1])
                            cbow_trains.extend(batch[2])
                            cbow_labels.extend(batch[3])
                            batch_size += 1
                            if batch_size % mimi_batch == 0:
                                newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                            np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)])
                                sg_trains = []
                                sg_labels = []
                                cbow_trains = []
                                cbow_labels = []
                                
                    newBatches.append([np.array(sg_trains,dtype=np.int32),np.array(sg_labels,dtype=np.int32),
                                            np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)]) 
                return batches,newBatches

def generate_sg_batch_position(patients, window, data_index, only_dx_flag, num_skips, skip_window):
   
    batch = list()
    labels = list()

    patient = patients[data_index]
    visits = patient['visits']

    visits_sorted = sorted(visits,key = lambda visit: visit['admsn_dt']) 

    for pos, visit in enumerate(visits_sorted):
        
        window_pos = enumerate(visits_sorted[pos:(pos + window)], pos)
        selected_visits = [c_visit for pos2, c_visit in window_pos if (len(c_visit['DXs']) > 0)]
        
        selected_codes = []
        code_index = 0
        
        for s_visit in selected_visits:
            codes = s_visit['DXs']
            if not only_dx_flag:
                codes.extend(s_visit['CPTs'])
            selected_codes.extend(codes)
        size_selected_codes = len(selected_codes)
        
        span = 2 * skip_window + 1  # [ skip_window target skip_window ]
        buffer = collections.deque(maxlen=span)  # pylint: disable=redefined-builtin
        buffer.extend(selected_codes[code_index:code_index + span])
        code_index += span
        
        window_batch = []
        window_labels = []
        for i in range(size_selected_codes // num_skips):
            context_words = [w for w in range(span) if w != skip_window]
            words_to_use = random.sample(context_words, num_skips)
            for context_word in words_to_use:
                if context_word < len(buffer):
                    window_batch.append(buffer[skip_window])
                    window_labels.append([buffer[context_word]])
            if code_index >= size_selected_codes:
                buffer.extend(selected_codes[0:span])
                code_index = span
            else:
                buffer.append(selected_codes[code_index])
                code_index += 1
        batch.extend(window_batch)
        labels.extend(window_labels)
#     data_index = (data_index + 1) % len(patients)
    return np.array(batch,dtype=np.int32),np.array(labels,dtype=np.int32)

def generate_cbow_batch_attention(patients,data_index, only_dx_flag, skip_window, sample_flag, wordSample, mimi_batch):
   
    batches = []
    batches_list = []
    patient = patients[data_index]
    visits = patient['visits']

    selected_visits = sorted(visits,key = lambda visit: visit['admsn_dt']) 
    selected_codes = []
    
    #reduced window
    reduced_window = npRdm.RandomState(1).randint(skip_window)
#     reduced_window = 0
    #actual window
    actual_window = skip_window - reduced_window

    #concat all codes togeter for one patient, and the format is [code, date]
    for s_visit in selected_visits:
        dt = datetime.datetime.strptime(s_visit['admsn_dt'], "%Y%m%d")
        codes = s_visit['DXs']
        if not only_dx_flag:
            codes.extend(s_visit['CPTs'])
        for code in codes:
            selected_codes.append([code,dt])

    #sampling codes based on their frequncy in dataset
    if sample_flag:
        selected_codes = [code for code in selected_codes if wordSample[code[0]] > npRdm.rand() * 2 ** 32]

    #generate batch samples
    for pos, word in enumerate(selected_codes):
        
        cbow_trains = []
        cbow_labels = []
        
        # now go over all words from the actual window, predicting each one in turn
        start = max(0, pos - actual_window)
        window_pos = enumerate(selected_codes[start:(pos + actual_window + 1)], start)

        word2_indices = [[word2[0],np.abs((word2[1]-word[1]).days)] for pos2, word2 
                         in window_pos if (word2[0] is not None and pos2 != pos)]
#             print(word2_indices)
        cbow_trains.append(word2_indices)
        cbow_labels.append([word[0]])

        batches.append([np.array(window_cbow_trains,dtype=np.int32),np.array(window_cbow_labels,dtype=np.int32)])
        batches_list.append([window_cbow_trains,window_cbow_labels])     


    batch_size = len(batches)
    newBatches = []
    lengths = []
    for i in range(batch_size):
        lengths.append(batches[i][0].shape[0])
    uniLengths = set(lengths)

    for length in uniLengths:
        cbow_trains = []
        cbow_labels = []
        batch_size = 0
        for batch in batches_list:
            if len(batch[0]) == length:
                cbow_trains.extend(batch[0])
                cbow_labels.extend(batch[1])
                batch_size += 1
                if batch_size % mimi_batch == 0:
                    newBatches.append([np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)])
                    cbow_trains = []
                    cbow_labels = []

        newBatches.append([np.array(cbow_trains,dtype=np.int32),np.array(cbow_labels,dtype=np.int32)]) 
    return batches,newBatches
    