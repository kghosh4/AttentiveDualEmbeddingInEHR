import glob
import pandas as pd
import json
import datetime

cols = ['DESYNPUF_ID','CLM_FROM_DT','CLM_UTLZTN_DAY_CNT','ICD9_DGNS_CD_1','ICD9_DGNS_CD_2','ICD9_DGNS_CD_3','ICD9_DGNS_CD_4'
       ,'ICD9_DGNS_CD_5','ICD9_DGNS_CD_6','ICD9_DGNS_CD_7','ICD9_DGNS_CD_8','ICD9_DGNS_CD_9','ICD9_DGNS_CD_10'
       ,'ICD9_PRCDR_CD_1','ICD9_PRCDR_CD_2','ICD9_PRCDR_CD_3','ICD9_PRCDR_CD_4','ICD9_PRCDR_CD_5','ICD9_PRCDR_CD_6']

def processing_DESynPUF(source_path,output_file):
    
#     path ='../data/DESynPUF/inpatient' # use your path
    allFiles = glob.glob(source_path + "/*.csv")
    frame = pd.DataFrame()
    list_ = []
    for file_ in allFiles:
        df = pd.read_csv(file_,index_col=None, dtype=object, header=0)
        list_.append(df)
    frame = pd.concat(list_)
    
    data = frame[cols]
    
    unique_pats = data.DESYNPUF_ID.unique()
    
    grouped = data.groupby(['DESYNPUF_ID'])
    
#     samples = unique_pats[range(1000)]
    
    patients = []
    for sub_id in unique_pats:
        patient = dict()
        patient['pid'] = sub_id
        visits = []
        acts = grouped.get_group(sub_id)
        for index, row in acts.iterrows():
            act = dict()
            admsn_dt = row['CLM_FROM_DT']
            day_cnt = row['CLM_UTLZTN_DAY_CNT']
            #skip if admision or discharge data is null 
            if admsn_dt != admsn_dt:
                continue
            act['admsn_dt'] = admsn_dt
            act['day_cnt'] = day_cnt
            DXs = []
            for i in range(10):
                dx =  row['ICD9_DGNS_CD_'+str(i+1)]
                #if dx is not NaN
                if dx == dx:
                    DXs.append(dx)
            act['DXs'] = DXs
            CPTs = []
            for i in range(6):
                cpt =  row['ICD9_PRCDR_CD_'+str(i+1)]
                #if cpt is not NaN
                if cpt == cpt:
                    CPTs.append(cpt)
            act['CPTs'] = CPTs
            visits.append(act)
        patient['visits'] = visits
        patients.append(patient)
   
    with open(output_file, 'w') as outfile:
        json.dump(patients, outfile)
    
    return patients

def processing_mimic3(file_adm,file_dxx,file_txx,output_file):
    
#     file_adm = '../data/MIMIC3/ADMISSIONS.csv'
#     file_dxx = '../data/MIMIC3/DIAGNOSES_ICD.csv'
    m_adm = pd.read_csv(file_adm)
    m_dxx = pd.read_csv(file_dxx)
    m_txx = pd.read_csv(file_txx,dtype= {'ICD9_CODE':object})
    
    unique_pats = m_dxx.SUBJECT_ID.unique()
    
    patients = []
    for sub_id in unique_pats:
        patient = dict()
        patient['pid'] = str(sub_id)
        pat_dxx = m_dxx[m_dxx.SUBJECT_ID==sub_id]
        uni_hadm = pat_dxx.HADM_ID.unique()
        grouped = pat_dxx.groupby(['HADM_ID'])
        visits = []
        for hadm in uni_hadm:
            act = dict()
            adm = m_adm[(m_adm.SUBJECT_ID==sub_id) & (m_adm.HADM_ID==hadm)]
            admsn_dt = datetime.datetime.strptime(adm.ADMITTIME.values[0], "%Y-%m-%d %H:%M:%S")
            disch_dt = datetime.datetime.strptime(adm.DISCHTIME.values[0], "%Y-%m-%d %H:%M:%S")

            delta = disch_dt - admsn_dt
            act['admsn_dt'] = admsn_dt.strftime("%Y%m%d")
            act['day_cnt'] = str(delta.days+1)
            
            codes = grouped.get_group(hadm)
#             print(codes)
            DXs = []
            for index, row in codes.iterrows():
                dx =  row['ICD9_CODE']
                #if dx is not NaN
                if dx == dx:
                    DXs.append(dx)
                    
            TXs = []
            pat_txx = m_txx[(m_txx.SUBJECT_ID==sub_id) & ( m_txx.HADM_ID==hadm)]
            tx_codes = pat_txx.ICD9_CODE.values
            for code in tx_codes:
                if code == code:
                    TXs.append(code)

            act['DXs'] = DXs
            act['CPTs'] = TXs
            visits.append(act)
        patient['visits'] = visits
        patients.append(patient)
    
    with open(output_file, 'w') as outfile:
        json.dump(patients, outfile)
        
    return patients
