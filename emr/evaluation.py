from emr.icd9_ontology import ICD_Ontology as icd
from emr.icd9_ontology import CCS_Ontology as ccs
from sklearn.cluster import KMeans
from sklearn import metrics
import numpy as np

def clustering_eval(final_embeddings,reverse_dictionary,logging, only_dx_flag):
    
    dictionary = dict(zip(reverse_dictionary.values(), reverse_dictionary.keys()))
    code_list = list(reverse_dictionary.values())[1:]
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_'):
            dx_codes.append(code)
        elif code.startswith('T_'):
            tx_codes.append(code)
            
    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    icd9 = icd(icd_file,True)
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = icd9.getRootLevel(dx[2:])
        dx_weights[dx_index] = final_embeddings[dictionary[dx],:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (dx_uni_labels)
    print(log_str)
    logging.info(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    logging.info(log_str)
    print(log_str)
    
    if not only_dx_flag:
        icd_file = 'emr/D_ICD_PROCEDURES.csv'
        icd9 = icd(icd_file,False)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)
        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = icd9.getRootLevel(tx[2:])
            tx_weights[tx_index] = final_embeddings[dictionary[tx],:]
            tx_index += 1
            
        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in ICD9: %s" % (tx_uni_labels)
        print(log_str)
        logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
        logging.info(log_str)
        print(log_str)
    return round(mi, 4), round(nmi, 4)

def clustering_eval_ccs(final_embeddings,reverse_dictionary,logging, only_dx_flag):
    
    code_list = list(reverse_dictionary.values())[1:]
    dictionary = dict(zip(reverse_dictionary.values(), reverse_dictionary.keys()))
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_'):
            dx_codes.append(code)
        else:
            tx_codes.append(code)
            
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = ccs_icd9.getRootLevel(dx[2:])
        dx_weights[dx_index] = final_embeddings[dictionary[dx],:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (dx_uni_labels)
    print(log_str)
    logging.info(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    logging.info(log_str)
    print(log_str)  
    
    if not only_dx_flag:
        ccs_file = 'emr/SinglePR-edit.txt'       
        ccs_icd9 = ccs(ccs_file)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)

        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = ccs_icd9.getRootLevel(tx[2:])
            tx_weights[tx_index] = final_embeddings[dictionary[tx],:]
            tx_index += 1

        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in CCS: %s" % (tx_uni_labels)
        print(log_str)
        logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
        logging.info(log_str)
        print(log_str)  
        
    return round(mi, 4), round(nmi, 4)

def clustering_eval_NL(final_embeddings,dictionary, only_dx_flag):
    
    code_list = list(dictionary.keys())
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_') and len(code)>2:
            dx_codes.append(code)
        elif code.startswith('T_'):
            tx_codes.append(code)
            
    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    icd9 = icd(icd_file,True)
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = icd9.getRootLevel(dx[2:])
        dx_weights[dx_index] = final_embeddings[dictionary[dx],:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (dx_uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)
    
    if not only_dx_flag:
        icd_file = 'emr/D_ICD_PROCEDURES.csv'
        icd9 = icd(icd_file,False)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)
        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = icd9.getRootLevel(tx[2:])
            tx_weights[tx_index] = final_embeddings[dictionary[tx],:]
            tx_index += 1
            
        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in ICD9: %s" % (tx_uni_labels)
        print(log_str)
#         logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#         logging.info(log_str)
        print(log_str)
    
def clustering_eval_NL_2EM(embeddings1,embeddings2,dictionary,only_dx_flag, concat):
    
    code_list = list(dictionary.keys())
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_') and len(code)>2:
            dx_codes.append(code)
        elif code.startswith('T_'):
            tx_codes.append(code)
            
    em1 = np.loadtxt(embeddings1, delimiter=',')
    em2 = np.loadtxt(embeddings2, delimiter=',')
    if concat:
        final_embeddings = np.concatenate((em1, em2), axis=1)
    else:
        final_embeddings = em1 + em2
    
    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    icd9 = icd(icd_file,True)
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = icd9.getRootLevel(dx[2:])
        dx_weights[dx_index] = final_embeddings[int(dictionary[dx]),:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (dx_uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)
    
    if not only_dx_flag:
        icd_file = 'emr/D_ICD_PROCEDURES.csv'
        icd9 = icd(icd_file,False)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)
        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = icd9.getRootLevel(tx[2:])
            tx_weights[tx_index] = final_embeddings[int(dictionary[tx]),:]
            tx_index += 1
            
        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in ICD9: %s" % (tx_uni_labels)
        print(log_str)
#         logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#         logging.info(log_str)
        print(log_str)
    

def clustering_eval_ccs_NL(final_embeddings,dictionary, only_dx_flag):
    
    code_list = list(dictionary.keys())
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_') and len(code)>2:
            dx_codes.append(code)
        else:
            tx_codes.append(code)
            
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = ccs_icd9.getRootLevel(dx[2:].replace('.',''))
        dx_weights[dx_index] = final_embeddings[dictionary[dx],:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (dx_uni_labels)
    print(log_str)
#     logging.info(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)  
    
    if not only_dx_flag:
        ccs_file = 'emr/SinglePR-edit.txt'       
        ccs_icd9 = ccs(ccs_file)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)

        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = ccs_icd9.getRootLevel(tx[2:].replace('.',''))
            tx_weights[tx_index] = final_embeddings[dictionary[tx],:]
            tx_index += 1

        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in CCS: %s" % (tx_uni_labels)
        print(log_str)
#         logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#         logging.info(log_str)
        print(log_str)  

def clustering_eval_ccs_NL_2EM(embeddings1,embeddings2,dictionary, only_dx_flag, concat):
    
    code_list = list(dictionary.keys())
    dx_codes = list()
    tx_codes = list()
    for code in code_list:
        if code.startswith('D_') and len(code)>2:
            dx_codes.append(code)
        else:
            tx_codes.append(code)
            
    em1 = np.loadtxt(embeddings1, delimiter=',')
    em2 = np.loadtxt(embeddings2, delimiter=',')
    if concat:
        final_embeddings = np.concatenate((em1, em2), axis=1)
    else:
        final_embeddings = em1 + em2
            
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    
    dx_weights = np.empty((len(dx_codes),final_embeddings.shape[1]),float)
    dx_labels  = np.empty((len(dx_codes)), int)
    
    dx_index = 0
    for dx in dx_codes:
        dx_labels[dx_index] = ccs_icd9.getRootLevel(dx[2:].replace('.',''))
        dx_weights[dx_index] = final_embeddings[int(dictionary[dx]),:]
        dx_index += 1
    
    dx_uni_labels = np.unique(dx_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (dx_uni_labels)
    print(log_str)
#     logging.info(log_str)
    kmeans = KMeans(n_clusters=dx_uni_labels, random_state=42).fit(dx_weights)

    mi = metrics.mutual_info_score(dx_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(dx_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)  
    
    if not only_dx_flag:
        ccs_file = 'emr/SinglePR-edit.txt'       
        ccs_icd9 = ccs(ccs_file)

        tx_weights = np.empty((len(tx_codes),final_embeddings.shape[1]),float)
        tx_labels  = np.empty((len(tx_codes)), int)

        tx_index = 0
        for tx in tx_codes:
            tx_labels[tx_index] = ccs_icd9.getRootLevel(tx[2:].replace('.',''))
            tx_weights[tx_index] = final_embeddings[int(dictionary[tx]),:]
            tx_index += 1

        tx_uni_labels = np.unique(tx_labels).shape[0]
        log_str = "number of tx_labels in CCS: %s" % (tx_uni_labels)
        print(log_str)
#         logging.info(log_str)
        kmeans = KMeans(n_clusters=tx_uni_labels, random_state=42).fit(tx_weights)

        mi = metrics.mutual_info_score(tx_labels,kmeans.labels_)
        nmi = metrics.normalized_mutual_info_score(tx_labels,kmeans.labels_)

        log_str = "TX of clustering_eval_ccs, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#         logging.info(log_str)
        print(log_str) 

    
def clustering_eval_gensim(model_cbow, embedding_size):
    
    words_cnt = len(model_cbow.wv.vocab)
    weights = np.empty((words_cnt,embedding_size), float)
    icd_labels = np.empty((words_cnt), int)
    ccs_labels = np.empty((words_cnt), int)

    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    icd9 = icd(icd_file,True)

    index = 0
    for k in list(model_cbow.wv.vocab.keys()):

        weights[index] = model_cbow.wv[k]
        icd_labels[index] = icd9.getRootLevel(k[2:])
        ccs_labels[index] = ccs_icd9.getRootLevel(k[2:])
        index += 1

    
    uni_labels = np.unique(icd_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(icd_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(icd_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)
    
    uni_labels = np.unique(ccs_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(ccs_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(ccs_labels,kmeans.labels_)

    log_str = "DX of clustering_eval_CCS, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
#     logging.info(log_str)
    print(log_str)
    
def clustering_eval_glove(vectors_file):
    
    weights = []
    codes = []
    icd_labels = []
    ccs_labels = []

    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    icd9 = icd(icd_file,True)
    
    with open(vectors_file,'r') as read_file:
        for line in read_file:
            line2list = line.split()
            code = line2list[0]
            weight = line2list[1:]
            codes.append(code)
            weights.append(weight)
            
    codes.pop()
    weights.pop()
    for code in codes:
        icd_labels.append(icd9.getRootLevel(code[2:]))  
        ccs_labels.append(ccs_icd9.getRootLevel(code[2:]))
   
    icd_labels = np.array(icd_labels,dtype=int)
    ccs_labels = np.array(ccs_labels,dtype=int)
    weights = np.array(weights,dtype=float)

    uni_labels = np.unique(icd_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(icd_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(icd_labels,kmeans.labels_)
    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    print(log_str)
    
    uni_labels = np.unique(ccs_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(ccs_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(ccs_labels,kmeans.labels_)
    log_str = "DX of clustering_eval_CCS, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    print(log_str)  
    
    
def nns_eval_glove(vectors_file):
    
    weights = []
    codes = []
    icd_labels = []
    ccs_labels = []

    icd_file = 'emr/D_ICD_DIAGNOSES.csv'
    ccs_file = 'emr/SingleDX-edit.txt'        
    ccs_icd9 = ccs(ccs_file)
    icd9 = icd(icd_file,True)
    
    with open(vectors_file,'r') as read_file:
        for line in read_file:
            line2list = line.split()
            code = line2list[0]
            weight = line2list[1:]
            codes.append(code)
            weights.append(weight)
            
    codes.pop()
    weights.pop()
    for code in codes:
        icd_labels.append(icd9.getRootLevel(code[2:]))  
        ccs_labels.append(ccs_icd9.getRootLevel(code[2:]))
   
    icd_labels = np.array(icd_labels,dtype=int)
    ccs_labels = np.array(ccs_labels,dtype=int)
    weights = np.array(weights,dtype=float)

    uni_labels = np.unique(icd_labels).shape[0]
    log_str = "number of dx_labels in ICD9: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(icd_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(icd_labels,kmeans.labels_)
    log_str = "DX of clustering_eval_ICD, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    print(log_str)
    
    uni_labels = np.unique(ccs_labels).shape[0]
    log_str = "number of dx_labels in CCS: %s" % (uni_labels)
    print(log_str)
    kmeans = KMeans(n_clusters=uni_labels, random_state=42).fit(weights)

    mi = metrics.mutual_info_score(ccs_labels,kmeans.labels_)
    nmi = metrics.normalized_mutual_info_score(ccs_labels,kmeans.labels_)
    log_str = "DX of clustering_eval_CCS, MI Score:%s, NMI Score:%s" % (round(mi, 4), round(nmi, 4))
    print(log_str)  
    
    