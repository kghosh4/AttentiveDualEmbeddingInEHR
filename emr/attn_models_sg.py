import tensorflow as tf
import math
import time
from emr.attn_data_gen_sg import generate_sg_batch_position
from emr.attn_data_gen_sg import generate_sg_batch_position_gensim
from emr.attn_data_gen_sg import generate_sg_cbow_batch_position_gensim
from emr.attn_data_gen_sg import generate_sg_cbow_batch_attention
from emr.evaluation_SA import clustering_eval,clustering_eval_ccs
import numpy as np

def build_graph(config,
                epoch,
                embedding_size,
                vocabulary_size,
                reverse_dictionary,
                num_sampled,
                valid_examples,
                patients,
                window,
                valid_size,
                logging,
                gpu_device,
                diag_flag,
                only_dx_flag,skip_window,sample_flag,wordSample,log_dir):
    
    graph = tf.Graph()
    num_data = len(patients)
    with graph.as_default(), tf.device(gpu_device):

        # Input data.
        train_inputs = tf.placeholder(tf.int32,shape=[None])
        train_labels = tf.placeholder(tf.int32, shape=[None, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Ops and variables pinned to the CPU because of missing GPU implementation
        with tf.device('/cpu:0'):
            # Look up embeddings for inputs.
            with tf.name_scope('embeddings'):
                init_embed = tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0)
                embeddings = tf.Variable(init_embed)

                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                embed = tf.nn.embedding_lookup(embeddings, train_inputs)

            # Construct the variables for the NCE loss
            with tf.name_scope('weights'):
                nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('biases'):
                nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
           

        # Compute the average NCE loss for the batch.
        # tf.nce_loss automatically draws a new sample of the negative labels each
        # time we evaluate the loss.
        start = time.time()
        
#         with tf.device('/cpu:0'):
        with tf.name_scope('loss'): 
            loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = nce_weights, 
                            biases = nce_biases,  
                            labels = train_labels,
                            inputs = embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )

        # Add the loss value as a scalar to summary.
        tf.summary.scalar('loss', loss)
        
        # Construct the SGD optimizer using a learning rate of 1.0.
        with tf.name_scope('optimizer'):
            optimizer = tf.train.AdamOptimizer().minimize(loss)

        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims =True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)
        similarity = tf.matmul( valid_embeddings, normalized_embeddings, transpose_b=True)
        
        # Merge all summaries.
        merged = tf.summary.merge_all()
        
        # Add variable initializer.
        init = tf.global_variables_initializer()

        # Create a saver.
        saver = tf.train.Saver()


    #training model
    with tf.Session(graph=graph, config=config) as session:
        # We must initialize all variables before we use them.
         # Add variable initializer.
        writer = tf.summary.FileWriter(log_dir, session.graph)
        
        init.run()
        logging.info('valid_examples: %s' % valid_examples)
        print("Initialized") 
        HyperParameters = "  epoch:%s,\
                       window:%s, \
                       embedding_size:%s, \
                       negative_sampled:%s,\
                       skip_window:%s" % \
                      (epoch, window,embedding_size,num_sampled,skip_window)
        logging.info(HyperParameters)
       
        average_loss = 0
        for e in range(epoch):
            step = 0
            for n in range(num_data):
                batches = generate_sg_batch_position_gensim(patients, window, n, only_dx_flag, skip_window, sample_flag, wordSample)
                
                if len(batches[0]) > 0:
                    try:
                        feed_dict = {train_inputs : batches[0], train_labels : batches[1]}

                        # Define metadata variable.
                        run_metadata = tf.RunMetadata()

                        # We perform one update step by evaluating the optimizer op (including it
                        # in the list of returned values for session.run()
                        summary, loss_val = session.run([optimizer, loss], feed_dict=feed_dict, run_metadata=run_metadata)
                        average_loss += loss_val

                        # Add returned summaries to writer in each step.
    #                             writer.add_summary(summary, step)

                        if step % 2000 == 0:
                            if step > 0:
                                average_loss /= 2000
                            # The average loss is an estimate of the loss over the last 2000 batches.
                            print("Average loss at step ", step, ": ", average_loss)
                            logging.info("Average loss at step %s: %s" % (step,average_loss))
                            average_loss = 0

                        # Note that this is expensive (~20% slowdown if computed every 500 steps)
                        if step % 50000 == 0:
                             # Add returned summaries to writer in each step.
#                             writer.add_summary(summary, step)
#                             sim = similarity.eval()
#                             for i in range(valid_size):
#                                 valid_word = reverse_dictionary[valid_examples[i]]
#                                 top_k = 8 # number of nearest neighbors
#                                 nearest = (-sim[i, :]).argsort()[1:top_k+1]
#                                 log_str = "Nearest to %s:" % valid_word
#                                 for k in range(top_k):
#                                     close_word = reverse_dictionary[nearest[k]]
#                                     log_str = "%s %s," % (log_str, close_word)
#                                 print(log_str)
#                                 logging.info(log_str)
                            mid_embeddings = normalized_embeddings.eval()
                            clustering_eval(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                            clustering_eval_ccs(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                            logging.info("Number of Epoch: %s," % e)
                        step += 1
                    except ValueError:
                        print(batches)
                        logging.info("Error in batches: %s," % batches)
                        logging.info("Number of Epoch: %s," % e)
                        pass
        final_embeddings = normalized_embeddings.eval()
        done = time.time()
        interval_mins = (done - start)/60
        logging.info("Start time: %s," % start)
        logging.info("End time: %s," % done)
        logging.info("Number of Epoch: %s," % e)
        log_str = "Model Running tims in minuts: %s," % interval_mins
        print(log_str)
        logging.info(log_str)
        
        clustering_eval(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        clustering_eval_ccs(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        
    return final_embeddings


def build_graph_SG_CBOW(config,
                epoch,
                embedding_size,
                vocabulary_size,
                reverse_dictionary,
                num_sampled,
                valid_examples,
                patients,
                window,
                valid_size,
                logging,
                gpu_device,
                diag_flag,
                only_dx_flag,
                        skip_window,
                        sample_flag,
                        wordSample,
                        window_batch,
                        mimi_batch,
                       sg = 2,dataset):
    
    graph = tf.Graph()
    num_data = len(patients)
    with graph.as_default(), tf.device(gpu_device):

        # Input data.
        sg_train_inputs = tf.placeholder(tf.int32,shape=[None])
        sg_train_labels = tf.placeholder(tf.int32, shape=[None, 1])        
        if window_batch:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[1,None])
            cbow_train_labels = tf.placeholder(tf.int32, shape=[1, 1])
        else:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[None,None])
            cbow_train_labels = tf.placeholder(tf.int32, shape=[None, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Ops and variables pinned to the CPU because of missing GPU implementation
        with tf.device('/cpu:0'):
            # Look up embeddings for inputs.
            with tf.name_scope('embeddings'):
                init_embed = tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0)
                embeddings = tf.Variable(init_embed)
            with tf.name_scope('sg_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                sg_embed = tf.nn.embedding_lookup(embeddings, sg_train_inputs)

            # Construct the variables for the NCE loss
            with tf.name_scope('sg_weights'):
                sg_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('sg_biases'):
                sg_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
                
            with tf.name_scope('cbow_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                cbow_embed = tf.nn.embedding_lookup(embeddings, cbow_train_inputs)
                cbow_embed = tf.reduce_mean(cbow_embed, 1)
                
            # Construct the variables for the NCE loss
            with tf.name_scope('cbow_weights'):
                cbow_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('cbow_biases'):
                cbow_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
           

        # Compute the average NCE loss for the batch.
        # tf.nce_loss automatically draws a new sample of the negative labels each
        # time we evaluate the loss.
        start = time.time()
        
#         with tf.device('/cpu:0'):
        with tf.name_scope('sg_loss'): 
            sg_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = sg_nce_weights, 
                            biases = sg_nce_biases,  
                            labels = sg_train_labels,
                            inputs = sg_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )
        with tf.name_scope('cbow_loss'): 
            cbow_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = cbow_nce_weights, 
                            biases = cbow_nce_biases,  
                            labels = cbow_train_labels,
                            inputs = cbow_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )

        # Add the loss value as a scalar to summary.
        tf.summary.scalar('sg_loss', sg_loss)
        tf.summary.scalar('cbow_loss', cbow_loss)
        
        # Construct the SGD optimizer using a learning rate of 1.0.
        with tf.name_scope('sg_optimizer'):
            sg_optimizer = tf.train.AdamOptimizer().minimize(sg_loss)
        with tf.name_scope('cbow_optimizer'):
            cbow_optimizer = tf.train.AdamOptimizer().minimize(cbow_loss)    


        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims =True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)
        similarity = tf.matmul( valid_embeddings, normalized_embeddings, transpose_b=True)
        
        # Merge all summaries.
        merged = tf.summary.merge_all()
        
        # Add variable initializer.
        init = tf.global_variables_initializer()

        # Create a saver.
        saver = tf.train.Saver()


    #training model
    with tf.Session(graph=graph, config=config) as session:
        # We must initialize all variables before we use them.
         # Add variable initializer.
#         writer = tf.summary.FileWriter(FLAGS.log_dir, session.graph)
        
        init.run()
        logging.info('valid_examples: %s' % valid_examples)
        print("Initialized") 
        HyperParameters = "  epoch:%s,\
                       window:%s, \
                       embedding_size:%s, \
                       negative_sampled:%s,\
                       skip_window:%s" % \
                      (epoch, window,embedding_size,num_sampled,skip_window)
        logging.info(HyperParameters)
       
        average_loss = 0
        for e in range(epoch):
            step = 0
            for n in range(num_data):
                window_batches,group_batches = generate_sg_cbow_batch_position_gensim(patients, 
                                                                                      window, n,
                                                                                      only_dx_flag,
                                                                                      skip_window,
                                                                                      sample_flag,
                                                                                      wordSample,
                                                                                      mimi_batch)
                
                if window_batch:
                    batches = window_batches
                else:
                    batches = group_batches
                    
                for batch in batches:
                    if len(batch[0]) > 0:
                        try:
                            feed_dict = {sg_train_inputs : batch[0], sg_train_labels : batch[1],
                                         cbow_train_inputs : batch[2], cbow_train_labels : batch[3]}

                            # Define metadata variable.
                            run_metadata = tf.RunMetadata()

                            # We perform one update step by evaluating the optimizer op (including it
                            # in the list of returned values for session.run()
                            if sg == 2:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], feed_dict=feed_dict, run_metadata=run_metadata)
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += (cbow_loss_val+sg_loss_val)
                            elif sg == 0:
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += cbow_loss_val
                            else:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += sg_loss_val

                            # Add returned summaries to writer in each step.
        #                             writer.add_summary(summary, step)

                            if step % 2000 == 0:
                                if step > 0:
                                    average_loss /= 2000
                                # The average loss is an estimate of the loss over the last 2000 batches.
                                print("Average loss at step ", step, ": ", average_loss)
                                logging.info("Average loss at step %s: %s" % (step,average_loss))
                                average_loss = 0

#                             # Note that this is expensive (~20% slowdown if computed every 500 steps)
                            if step % 50000 == 0:
#                                 sim = similarity.eval()
#                                 for i in range(valid_size):
#                                     valid_word = reverse_dictionary[valid_examples[i]]
#                                     top_k = 8 # number of nearest neighbors
#                                     nearest = (-sim[i, :]).argsort()[1:top_k+1]
#                                     log_str = "Nearest to %s:" % valid_word
#                                     for k in range(top_k):
#                                         close_word = reverse_dictionary[nearest[k]]
#                                         log_str = "%s %s," % (log_str, close_word)
#                                     print(log_str)
#                                     logging.info(log_str)
                                mid_embeddings = normalized_embeddings.eval()
                                clustering_eval(mid_embeddings,reverse_dictionary)
                                clustering_eval_ccs(mid_embeddings,reverse_dictionary)
#                                 clustering_eval(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
#                                 clustering_eval_ccs(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                                logging.info("Number of Epoch: %s," % e)
#                                 if sg == 0:
#                                     np.savetxt('outputs/cbow_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
#                                 elif sg == 1:
#                                     np.savetxt('outputs/sg_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
                            step += 1
                        except ValueError:
                            print(batches)
                            logging.info("Error in batches: %s," % batches)
                            logging.info("Number of Epoch: %s," % e)
                            pass
        final_embeddings = normalized_embeddings.eval()
        done = time.time()
        interval_mins = (done - start)/60
        logging.info("Start time: %s," % start)
        logging.info("End time: %s," % done)
        logging.info("Number of Epoch: %s," % e)
        log_str = "Model Running tims in minuts: %s," % interval_mins
        print(log_str)
        logging.info(log_str)
        
        clustering_eval(final_embeddings,reverse_dictionary)
        clustering_eval_ccs(final_embeddings,reverse_dictionary)
#                 clustering_eval(final_embeddings,reverse_dictionary,logging,only_dx_flag)
#         clustering_eval_ccs(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        
        if sg == 0:
            np.savetxt('outputs/'+dataset+'_cbow_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'.vect', final_embeddings, delimiter=',')
        elif sg == 1:
            np.savetxt('outputs/'+dataset+'_sg_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'.vect', final_embeddings, delimiter=',')
        
    return final_embeddings


def build_graph_SG_CBOW_Attention(config,
                                    epoch,
                                    embedding_size,
                                    vocabulary_size,
                                    reverse_dictionary,
                                    num_sampled,
                                    valid_examples,
                                    patients,
                                    window,
                                    valid_size,
                                    logging,
                                    gpu_device,
                                    diag_flag,
                                    only_dx_flag,
                                    skip_window,
                                    sample_flag,
                                    wordSample,
                                    k,
                                    window_batch,
                                    mimi_batch,
                                    sg = 2):
    
    graph = tf.Graph()
    num_data = len(patients)
    with graph.as_default(), tf.device(gpu_device):

        # Input data.
        sg_train_inputs = tf.placeholder(tf.int32,shape=[None])
        sg_train_labels = tf.placeholder(tf.int32,shape=[None, 1])
        
        if window_batch:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[1,None,2])
            cbow_train_labels = tf.placeholder(tf.int32,shape=[1, 1])
        else:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[None,None,2])
            cbow_train_labels = tf.placeholder(tf.int32,shape=[None, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Ops and variables pinned to the CPU because of missing GPU implementation
        with tf.device('/cpu:0'):
            # Look up embeddings for inputs.
            with tf.name_scope('embeddings'):
                init_embed = tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0)
                embeddings = tf.Variable(init_embed)
            with tf.name_scope('sg_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                sg_embed = tf.nn.embedding_lookup(embeddings, sg_train_inputs)

            # Construct the variables for the NCE loss
            with tf.name_scope('sg_weights'):
                sg_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('sg_biases'):
                sg_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
                
            with tf.name_scope('cbow_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                cbow_embed = tf.nn.embedding_lookup(embeddings, cbow_train_inputs[:,:,0])
                
                attn_weights = tf.Variable(tf.truncated_normal([embedding_size,k],stddev=1.0 / math.sqrt(k)))
                attn_biases = tf.Variable(tf.zeros([k]))
                #weight add bias
                attn_embed = tf.nn.bias_add(attn_weights,attn_biases)
                #multiplying it with Ei
                attn_scalars = tf.tensordot(cbow_embed,attn_embed,axes = [[2], [0]])
                #get abs of distance
                train_delta = cbow_train_inputs[:,:,1]

                #distance function is log(dist+1)
                dist_fun = tf.log(tf.to_float(train_delta)+1.0)
                #reshape the dist_fun
                dist_fun = tf.reshape(dist_fun,[tf.shape(dist_fun)[0],tf.shape(dist_fun)[1],1])
                #the attribution logits
                attn_logits = tf.multiply(attn_scalars,dist_fun)
                
                # the attribution logits sum
                attn_logits_sum = tf.reduce_sum(attn_logits,-1,keepdims=True)
                #get weights via softmax
                attn_softmax = tf.nn.softmax(attn_logits_sum,1)

                #the weighted sum
                attn_embed_weighted = tf.multiply(attn_softmax,cbow_embed)
                reduced_embed = tf.reduce_sum(attn_embed_weighted,1)
                #obtain two scalars
                scalar1 = tf.log(tf.to_float(tf.shape(cbow_embed)[1])+1.0)
                scalar2 = tf.reduce_sum(tf.pow(attn_softmax,2),1)
                #the scalared embed
                reduced_embed = tf.multiply(reduced_embed,scalar1)
                reduced_embed = tf.multiply(reduced_embed,scalar2)
                
                cbow_embed = reduced_embed
                
            # Construct the variables for the NCE loss
            with tf.name_scope('cbow_weights'):
                cbow_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('cbow_biases'):
                cbow_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
           

        # Compute the average NCE loss for the batch.
        # tf.nce_loss automatically draws a new sample of the negative labels each
        # time we evaluate the loss.
        start = time.time()
        
#         with tf.device('/cpu:0'):
        with tf.name_scope('sg_loss'): 
            sg_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = sg_nce_weights, 
                            biases = sg_nce_biases,  
                            labels = sg_train_labels,
                            inputs = sg_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )
        with tf.name_scope('cbow_loss'): 
            cbow_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = cbow_nce_weights, 
                            biases = cbow_nce_biases,  
                            labels = cbow_train_labels,
                            inputs = cbow_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )

        # Add the loss value as a scalar to summary.
        tf.summary.scalar('sg_loss', sg_loss)
        tf.summary.scalar('cbow_loss', cbow_loss)
        
        # Construct the SGD optimizer using a learning rate of 1.0.
        with tf.name_scope('sg_optimizer'):
            sg_optimizer = tf.train.AdamOptimizer().minimize(sg_loss)
        with tf.name_scope('cbow_optimizer'):
            cbow_optimizer = tf.train.AdamOptimizer().minimize(cbow_loss)    


        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims =True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)
        similarity = tf.matmul( valid_embeddings, normalized_embeddings, transpose_b=True)
        
        # Merge all summaries.
        merged = tf.summary.merge_all()
        
        # Add variable initializer.
        init = tf.global_variables_initializer()

        # Create a saver.
        saver = tf.train.Saver()


    #training model
    with tf.Session(graph=graph, config=config) as session:
        # We must initialize all variables before we use them.
         # Add variable initializer.
#         writer = tf.summary.FileWriter(FLAGS.log_dir, session.graph)
        
        init.run()
        logging.info('valid_examples: %s' % valid_examples)
        print("Initialized") 
        HyperParameters = "  epoch:%s,\
                       window:%s, \
                       embedding_size:%s, \
                       negative_sampled:%s,\
                       skip_window:%s" % \
                      (epoch, window,embedding_size,num_sampled,skip_window)
        logging.info(HyperParameters)
       
        average_loss = 0
        for e in range(epoch):
            step = 0
            for n in range(num_data):
                window_batches,group_batches = generate_sg_cbow_batch_attention(patients, window, n, 
                                                                         only_dx_flag,skip_window,sample_flag,wordSample,mimi_batch)
                if window_batch:
                    batches = window_batches
                else:
                    batches = group_batches
                    
                for batch in batches:
                    if len(batch[0]) > 0:
                        try:
                            feed_dict = {sg_train_inputs : batch[0], sg_train_labels : batch[1],
                                         cbow_train_inputs : batch[2], cbow_train_labels : batch[3]}

                            # Define metadata variable.
                            run_metadata = tf.RunMetadata()

                            # We perform one update step by evaluating the optimizer op (including it
                            # in the list of returned values for session.run()
                            
                            if sg == 2:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += (cbow_loss_val+sg_loss_val)
                            
                            elif sg == 0:
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += cbow_loss_val
                            else:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += sg_loss_val

                            # Add returned summaries to writer in each step.
        #                             writer.add_summary(summary, step)

                            if step % 2000 == 0:
                                if step > 0:
                                    average_loss /= 2000
                                # The average loss is an estimate of the loss over the last 2000 batches.
                                print("Average loss at step ", step, ": ", average_loss)
                                logging.info("Average loss at step %s: %s" % (step,average_loss))
                                average_loss = 0

                            # Note that this is expensive (~20% slowdown if computed every 500 steps)
                            if step % 50000 == 0:
                                mid_embeddings = normalized_embeddings.eval()
                                clustering_eval(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                                clustering_eval_ccs(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                                logging.info("Number of Epoch: %s," % e)
                                
#                                 if sg == 0:
#                                     np.savetxt('outputs/cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
#                                 elif sg == 1:
#                                     np.savetxt('outputs/sg_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
                                
                            step += 1
                        except ValueError:
                            print(batches)
                            logging.info("Error in batches: %s," % batches)
                            logging.info("Number of Epoch: %s," % e)
                            pass
        final_embeddings = normalized_embeddings.eval()
        done = time.time()
        interval_mins = (done - start)/60
        logging.info("Start time: %s," % start)
        logging.info("End time: %s," % done)
        logging.info("Number of Epoch: %s," % e)
        log_str = "Model Running tims in minuts: %s," % interval_mins
        print(log_str)
        logging.info(log_str)
        
        clustering_eval(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        clustering_eval_ccs(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        
        if sg == 0:
            np.savetxt('outputs/cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        elif sg == 1:
            np.savetxt('outputs/sg_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        else:
            np.savetxt('outputs/sg-cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        
    return final_embeddings



def build_graph_CBOW_SA(config,
                                    epoch,
                                    embedding_size,
                                    vocabulary_size,
                                    reverse_dictionary,
                                    num_sampled,
                                    valid_examples,
                                    patients,
                                    window,
                                    valid_size,
                                    logging,
                                    gpu_device,
                                    diag_flag,
                                    only_dx_flag,
                                    skip_window,
                                    sample_flag,
                                    wordSample,
                                    k,
                                    window_batch,
                                    mimi_batch,
                                    sg = 2):
    
    graph = tf.Graph()
    num_data = len(patients)
    with graph.as_default(), tf.device(gpu_device):

        # Input data.
        sg_train_inputs = tf.placeholder(tf.int32,shape=[None])
        sg_train_labels = tf.placeholder(tf.int32,shape=[None, 1])
        
        if window_batch:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[1,None,2])
            cbow_train_labels = tf.placeholder(tf.int32,shape=[1, 1])
        else:
            cbow_train_inputs = tf.placeholder(tf.int32,shape=[None,None,2])
            cbow_train_labels = tf.placeholder(tf.int32,shape=[None, 1])
        valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

        # Ops and variables pinned to the CPU because of missing GPU implementation
        with tf.device('/cpu:0'):
            # Look up embeddings for inputs.
            with tf.name_scope('embeddings'):
                init_embed = tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0)
                embeddings = tf.Variable(init_embed)
            with tf.name_scope('sg_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                sg_embed = tf.nn.embedding_lookup(embeddings, sg_train_inputs)

            # Construct the variables for the NCE loss
            with tf.name_scope('sg_weights'):
                sg_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('sg_biases'):
                sg_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
                
            with tf.name_scope('cbow_embeddings'):
                # Embedding size is calculated as shape(train_inputs) + shape(embeddings)[1:]
                cbow_embed = tf.nn.embedding_lookup(embeddings, cbow_train_inputs[:,:,0])
                
                attn_weights = tf.Variable(tf.truncated_normal([embedding_size,k],stddev=1.0 / math.sqrt(k)))
                attn_biases = tf.Variable(tf.zeros([k]))
                #weight add bias
                attn_embed = tf.nn.bias_add(attn_weights,attn_biases)
                #multiplying it with Ei
                attn_scalars = tf.tensordot(cbow_embed,attn_embed,axes = [[2], [0]])
                #get abs of distance
                train_delta = cbow_train_inputs[:,:,1]

                #distance function is log(dist+1)
                dist_fun = tf.log(tf.to_float(train_delta)+1.0)
                #reshape the dist_fun
                dist_fun = tf.reshape(dist_fun,[tf.shape(dist_fun)[0],tf.shape(dist_fun)[1],1])
                #the attribution logits
                attn_logits = tf.multiply(attn_scalars,dist_fun)
                
                # the attribution logits sum
                attn_logits_sum = tf.reduce_sum(attn_logits,-1,keepdims=True)
                #get weights via softmax
                attn_softmax = tf.nn.softmax(attn_logits_sum,1)

                #the weighted sum
                attn_embed_weighted = tf.multiply(attn_softmax,cbow_embed)
                reduced_embed = tf.reduce_sum(attn_embed_weighted,1)
                #obtain two scalars
                scalar1 = tf.log(tf.to_float(tf.shape(cbow_embed)[1])+1.0)
                scalar2 = tf.reduce_sum(tf.pow(attn_softmax,2),1)
                #the scalared embed
                reduced_embed = tf.multiply(reduced_embed,scalar1)
                reduced_embed = tf.multiply(reduced_embed,scalar2)
                
                cbow_embed = reduced_embed
                
            # Construct the variables for the NCE loss
            with tf.name_scope('cbow_weights'):
                cbow_nce_weights = tf.Variable(
                    tf.truncated_normal([vocabulary_size, embedding_size],
                                        stddev=1.0 / math.sqrt(embedding_size)))
            with tf.name_scope('cbow_biases'):
                cbow_nce_biases = tf.Variable(tf.zeros([vocabulary_size]))
           

        # Compute the average NCE loss for the batch.
        # tf.nce_loss automatically draws a new sample of the negative labels each
        # time we evaluate the loss.
        start = time.time()
        
#         with tf.device('/cpu:0'):
        with tf.name_scope('sg_loss'): 
            sg_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = sg_nce_weights, 
                            biases = sg_nce_biases,  
                            labels = sg_train_labels,
                            inputs = sg_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )
        with tf.name_scope('cbow_loss'): 
            cbow_loss = tf.reduce_mean(
                        tf.nn.nce_loss(
                            weights = cbow_nce_weights, 
                            biases = cbow_nce_biases,  
                            labels = cbow_train_labels,
                            inputs = cbow_embed,
                            num_sampled = num_sampled, 
                            num_classes = vocabulary_size)
                            )

        # Add the loss value as a scalar to summary.
        tf.summary.scalar('sg_loss', sg_loss)
        tf.summary.scalar('cbow_loss', cbow_loss)
        
        # Construct the SGD optimizer using a learning rate of 1.0.
        with tf.name_scope('sg_optimizer'):
            sg_optimizer = tf.train.AdamOptimizer().minimize(sg_loss)
        with tf.name_scope('cbow_optimizer'):
            cbow_optimizer = tf.train.AdamOptimizer().minimize(cbow_loss)    


        # Compute the cosine similarity between minibatch examples and all embeddings.
        norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keepdims =True))
        normalized_embeddings = embeddings / norm
        valid_embeddings = tf.nn.embedding_lookup(normalized_embeddings, valid_dataset)
        similarity = tf.matmul( valid_embeddings, normalized_embeddings, transpose_b=True)
        
        # Merge all summaries.
        merged = tf.summary.merge_all()
        
        # Add variable initializer.
        init = tf.global_variables_initializer()

        # Create a saver.
        saver = tf.train.Saver()


    #training model
    with tf.Session(graph=graph, config=config) as session:
        # We must initialize all variables before we use them.
         # Add variable initializer.
#         writer = tf.summary.FileWriter(FLAGS.log_dir, session.graph)
        
        init.run()
        logging.info('valid_examples: %s' % valid_examples)
        print("Initialized") 
        HyperParameters = "  epoch:%s,\
                       window:%s, \
                       embedding_size:%s, \
                       negative_sampled:%s,\
                       skip_window:%s" % \
                      (epoch, window,embedding_size,num_sampled,skip_window)
        logging.info(HyperParameters)
       
        average_loss = 0
        for e in range(epoch):
            step = 0
            for n in range(num_data):
                window_batches,group_batches = generate_sg_cbow_batch_attention(patients, window, n, 
                                                                         only_dx_flag,skip_window,sample_flag,wordSample,mimi_batch)
                if window_batch:
                    batches = window_batches
                else:
                    batches = group_batches
                    
                for batch in batches:
                    if len(batch[0]) > 0:
                        try:
                            feed_dict = {sg_train_inputs : batch[0], sg_train_labels : batch[1],
                                         cbow_train_inputs : batch[2], cbow_train_labels : batch[3]}

                            # Define metadata variable.
                            run_metadata = tf.RunMetadata()

                            # We perform one update step by evaluating the optimizer op (including it
                            # in the list of returned values for session.run()
                            
                            if sg == 2:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += (cbow_loss_val+sg_loss_val)
                            
                            elif sg == 0:
                                _, cbow_loss_val = session.run([cbow_optimizer, cbow_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += cbow_loss_val
                            else:
                                _, sg_loss_val = session.run([sg_optimizer, sg_loss], 
                                                               feed_dict=feed_dict, run_metadata=run_metadata)
                                average_loss += sg_loss_val

                            # Add returned summaries to writer in each step.
        #                             writer.add_summary(summary, step)

                            if step % 2000 == 0:
                                if step > 0:
                                    average_loss /= 2000
                                # The average loss is an estimate of the loss over the last 2000 batches.
                                print("Average loss at step ", step, ": ", average_loss)
                                logging.info("Average loss at step %s: %s" % (step,average_loss))
                                average_loss = 0

                            # Note that this is expensive (~20% slowdown if computed every 500 steps)
                            if step % 50000 == 0:
                                mid_embeddings = normalized_embeddings.eval()
                                clustering_eval(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                                clustering_eval_ccs(mid_embeddings,reverse_dictionary,logging,only_dx_flag)
                                logging.info("Number of Epoch: %s," % e)
                                
#                                 if sg == 0:
#                                     np.savetxt('outputs/cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
#                                 elif sg == 1:
#                                     np.savetxt('outputs/sg_sk_'+str(skip_window)+'_epoch_'+str(e)+'.vect', mid_embeddings, delimiter=',')
                                
                            step += 1
                        except ValueError:
                            print(batches)
                            logging.info("Error in batches: %s," % batches)
                            logging.info("Number of Epoch: %s," % e)
                            pass
        final_embeddings = normalized_embeddings.eval()
        done = time.time()
        interval_mins = (done - start)/60
        logging.info("Start time: %s," % start)
        logging.info("End time: %s," % done)
        logging.info("Number of Epoch: %s," % e)
        log_str = "Model Running tims in minuts: %s," % interval_mins
        print(log_str)
        logging.info(log_str)
        
        clustering_eval(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        clustering_eval_ccs(final_embeddings,reverse_dictionary,logging,only_dx_flag)
        
        if sg == 0:
            np.savetxt('outputs/cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        elif sg == 1:
            np.savetxt('outputs/sg_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        else:
            np.savetxt('outputs/sg-cbow-attn_sk_'+str(skip_window)+'_epoch_'+str(epoch)+'_k_'+str(k)+'.vect', final_embeddings, delimiter=',')
        
    return final_embeddings