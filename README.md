## Paper ID: 254

## Title: Attentive Dual Embedding for Understanding Medical Concepts in Electronic Health Records
--------------------------------------------------------------------------------------------------

## Getting started

##Data Processing: 
1. data_preparation.py  --> Processes either the MIMIC-III data or the CMS data depending on whether the mimic_flag is set as True or False.
	
	•	For mimic_flag = False
		The cms data was processed and the output file “patients_cms_full.json” was generated. The data processing took around 2 mins. 

	•	For mimc_flag = True
		The mimic data was processed and the output file “patients_mimic3_full.json” was generated. The data processing took around 16 mins which was higher than the previous run due to the greater size of the input files. Only 5 MIMIC-III datasets were chosen as input during this execution.
		
		
	#Input: ADMISSIONS.csv, DIAGNOSES_ICD.csv, PROCEDURES_ICD.csv, PRESCRIPTIONS.csv, DRGCODES.csv
	
	#Output: patients_cms_full.json OR patients_mimic3_full.json (depending on whether mimic_flag is False or True)


2. mimic3-TF_SG_CBOW-Attn_K_change.ipynb  --> This is a Jupyter notebook.

	The output json file will be passed as input to this notebook.
	#Dependencies : attn_data_gen_sg.py, attn_models_sg.py
	
Note:
Please ensure that the dependent files are copied to the required directory as expected in the notebook. The dependent modules can be downloaded from the provide Git repository.

Also, we will have to have the Jupyter Notebook installed on my system so that we can run the ipynb notebook file using that.
To give you an example, I installed the Anaconda Naigator and that has all my pycharm (used for running .py codes) and Jupyter notebook (used to running .ipynb file)


##Citation to the original paper:
X. Peng, G. Long, S. Pan, J. Jiang and Z. Niu, "Attentive Dual Embedding for Understanding Medical Concepts in Electronic Health Records," 2019 International Joint Conference on Neural Networks (IJCNN), 2019, pp. 1-8, doi: 10.1109/IJCNN.2019.8852429.

## Link to the original paper’s repo:
The original paper didnt have any repository link. I received few of the codes from the original author via email.

## Table of Results:
Included in the Final Report.


## Data download instruction:
# MIMIC-III data:
	The MIMIC III data was downloaded from the below website:
	https://physionet.org/content/mimicdb/1.0.0/
	
	As approval request was sent prior to that so as to get the required approvals for downloading.

# CMS data:
	The CMS data was downloaded from the below website. A subset of the inpatient files for the period 2008 to 2010 has been selected for this paper.
	https://www.cms.gov

